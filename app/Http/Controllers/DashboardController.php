<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request){
        if (!$request->session()->has('user_id')) {
            return redirect('/');
        }
        $username = $request->session()->get('user_name');
        $title = "Dashboard";
        $v1 = view('dashboard.include.header',compact('username','title'));
        $v2 = view('dashboard.dashboard');
        $v3 = view('dashboard.include.footer');
        return $v1.$v2.$v3;
    }

    public function setting(Request $request){
        if (!$request->session()->has('user_id')) {
            return redirect('/');
        }
        $username = $request->session()->get('user_name');
        $title = "Profile Setting";
        $v1 = view('dashboard.include.header',compact('username','title'));
        $v2 = view('dashboard.setting');
        $v3 = view('dashboard.include.footer');
        return $v1.$v2.$v3;
    }
}
