<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(Request $request){
        if ($request->session()->has('error')) {
           $error = $request->session()->get('error');
           $request->session()->forget('error');
        }
        if ($request->session()->has('user_id')) {
            $request->session()->forget('error');
            return redirect('/dashboard');
        }else{
            return view('login',compact('error'));
        }
       
    }

    public function login(Request $request){
       
        $input = $request->all();
        $email =  $input['email'];
        $password =  $input['password'];
        $users = DB::table('users')->where([
            ['email', $email],
            ['password', $password],
            ['user_role', 1]
        ])->get();
        //$users = DB::table('users')->get();
        if(count($users) == 1){
            session(['user_id' => $users[0]->id]);
            session(['user_email' => $users[0]->email]);
            session(['user_name' => $users[0]->username]);
            return redirect('/dashboard');
            // $data = $request->session()->all();
            // return $data;
        }else{
            $request->session()->flush();
            $request->session()->flash('error', 'Error: Wrong Credential! Please check your Detail!!');
            return redirect('/');
        }
        
    }

    public function logout(Request $request){
        $request->session()->flush();
        return redirect('/');
    }
}
